/*

  This example provides an example of retrieving a secret from a local
  Vault instance utilizing an approle

  It assumes that:

    - You have set up a vault server, or have one to connect with
        - Set up the vault server:
          https://www.vaultproject.io/intro/getting-started/install.html
    - Added a secret
        - You can learn about setting up a first secret here:
          https://www.vaultproject.io/intro/getting-started/first-secret.html
        - ./vault write secret/hello value=world excited=yes

- Enable Approles:
        - ./vault auth-enable approle

- create a new role with some config info:
        - ./vault write auth/approle/role/testrole secret_id_ttl=10m token_num_uses=10 token_ttl=20m token_max_ttl=30m secret_id_num_uses=40

- create a new policy, in this case test-policy.hcl:
      path "secret/hello" {
        policy = "read"
      }

      path "secret/hello" {
        policy = "write"
      }

      - here are some instructions on policies: https://www.vaultproject.io/intro/getting-started/policies.html

- write the policy to vault: ./vault policy-write test-policy test-policy.hcl

- apply the policy to the role you created: ./vault write auth/approle/role/testrole policies=test-policy

- now all of the below code should work .. you'll need to add your own config

NOTE:  you can create a new token using the policy you created:
        - ./vault token-create -policy="test-policy"

NOTE:  If you want some logging, you can use this command:
        - ./vault audit-enable file file_path=/var/log/vault_audit.log
*/

import com.bettercloud.vault.VaultConfig
import com.bettercloud.vault.SslConfig
import com.bettercloud.vault.Vault
import com.bettercloud.vault.VaultException
import com.bettercloud.vault.response.LogicalResponse
import com.bettercloud.vault.api.Auth
import com.bettercloud.vault.response.AuthResponse

import com.bettercloud.vault.api.Logical;

// set up what we need for our config ... we are starting without a token
//def server = 'https://192.168.0.174'
def server = 'https://localhost'
def port = 8200
def openTimeout = 5
def readTimeout = 30
// Gotta start somewhere, this is the root token for the local vault server used to test this code
def token = 'bd19391c-68cc-7060-fda2-d2e59d11c28f'
def secret = 'secret/hello'
def keys = ['value', 'excited', 'test']

// set up the VaultConfig for our AppRole Auth Requests
final VaultConfig authConfig = new VaultConfig()
      .address( "$server:$port" )
      .token( token )
      .openTimeout( openTimeout )
      .readTimeout( readTimeout )
      //.sslConfig(new SslConfig().verify(false).build())// - if we are using TLS
      .sslConfig(new SslConfig().trustStoreResource("/keystore.jks").keyStoreResource("/keystore.jks", "changeit").build())
      .build()

Vault vaultAuth = new Vault(authConfig)

// get the Role ID for our Role
roleID = vaultAuth.logical().read('auth/approle/role/testrole/role-id').getData().get('role_id')

// Get the secret-id for the role, short form
secretID = vaultAuth.logical().write('auth/approle/role/testrole/secret-id', null)
            .getData().get('secret_id')

/*
  Here you can see another longer example with some more info:

  final LogicalResponse response = vault.logical().write('auth/approle/role/testrole/secret-id', null)
  String secretID = response.getData().get('secret_id')
  String secretIDAccessor = response.getData().get('secret_id_accessor')
  println "Secret ID: $secretID, SecretID Accessor: $secretIDAccessor"


*/

// Here is a short way of getting just what we need (the token):
authToken = vaultAuth.auth().loginByAppRole('approle', roleID, secretID).getAuthClientToken()

//test
println  vaultAuth.logical().read("auth/token/lookup/$authToken").getData().get('expire_time')

/*
  If we wanted to get a bunch of other info, long form, we could do it this way:

  final AuthResponse authResponse = vaultAuth.auth().loginByAppRole('approle', roleID, secretID)
  String authToken = authResponse.getAuthClientToken()
  boolean isRenewable = authResponse.isAuthRenewable()
  long leaseDuration = authResponse.getAuthLeaseDuration()
  String userName = authResponse.getUsername()
  boolean renewable = authResponse.getRenewable()
  List<String> policies = authResponse.getAuthPolicies()

  and then print them all out

  println "Auth Token: $authToken, renewable?: $isRenewable, Lease Duration: $leaseDuration, getRenewable$renewable, Policies: $policies"
*/

/*
  Now we can set up a new vault config for our requests, because we have a token:
*/
final VaultConfig requestsConfig = new VaultConfig()
      .address( "$server:$port" )
      .token( authToken )
      .openTimeout( openTimeout )
      .readTimeout( readTimeout )
      //.sslConfig(new SslConfig().verify(false).build())// - if we are using TLS
      .sslConfig(new SslConfig().trustStoreResource("/keystore.jks").keyStoreResource("/keystore.jks", "changeit").build())
      .build()

// If we wanted to use the direct logical we could, like this:
//Logical logicalOps = new Logical(logicalConfig)

// But instead we will use this:
Vault vaultRequests = new Vault(requestsConfig)

// iterate through our keys and get the values, if they exist
keys.each {
    // 'it' is a fun keyword for Groovy
    key = it
    value = vaultRequests.logical().read(secret).getData().get(key)
    //value = logicalOps.read(secret).getData().get(key)
    if (!value)
      println "'$it' is not present in the secret store"
    else
      println "$it = $value"
}
