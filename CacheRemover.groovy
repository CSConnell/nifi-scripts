import org.apache.commons.lang3.StringUtils
import org.apache.nifi.distributed.cache.client.DistributedMapCacheClient
import org.apache.nifi.distributed.cache.client.Serializer
import org.apache.nifi.distributed.cache.client.exception.SerializationException
import org.apache.nifi.distributed.cache.client.exception.DeserializationException
import org.apache.nifi.expression.AttributeExpression
import org.apache.nifi.flowfile.FlowFile
import org.apache.nifi.logging.ComponentLog
import org.apache.nifi.processor.Relationship
import org.apache.nifi.processor.exception.ProcessException
import org.apache.nifi.components.PropertyDescriptor
import org.apache.nifi.processor.util.StandardValidators
import org.apache.nifi.script.ScriptingComponentUtils



import java.nio.charset.StandardCharsets

class RemoveCache extends AbstractProcessor {

    public static final AllowableValue FULL = new AllowableValue("Full", "Full", "Will remove a single value based on the Full Identifier.")

    public static final AllowableValue PARTIAL = new AllowableValue("Partial", "Partial", "Will remove potentially multiple values based on the partial identifier.")

    public static final PropertyDescriptor PROP_DISTRIBUTED_CACHE_SERVICE = new PropertyDescriptor.Builder()
            .name("Distributed Cache Service")
            .description("The Controller Service that is used to get the cached values.")
            .required(true)
            .identifiesControllerService(DistributedMapCacheClient.class)
            .build()

    public static final PropertyDescriptor PROP_CACHE_ENTRY_IDENTIFIER = new PropertyDescriptor.Builder()
            .name("Cache Entry Identifier")
            .description("A FlowFile attribute, or the results of an Attribute Expression Language statement, which will be evaluated "
            + "against a FlowFile in order to determine the value used to identify duplicates; it is this value that is cached")
            .required(true)
            .addValidator(StandardValidators.createAttributeExpressionLanguageValidator(AttributeExpression.ResultType.STRING, true))
            .defaultValue('${id}')
            .expressionLanguageSupported(true)
            .build()

    public static final PropertyDescriptor PROP_CACHE_ENTRY_REMOVAL_TYPE = new PropertyDescriptor.Builder()
            .name("Cache Entry Removal Type")
            .description("Remove a duplicate based on the full or partial identifier. "
            + "The partial removes based on a pattern from the partial identifier, the full removes by the exact key.")
            .required(true)
//            .addValidator(StandardValidators.NON_EMPTY_VALIDATOR)
            .allowableValues(FULL, PARTIAL)
            .defaultValue(FULL.getValue())
            .build()

    public static final PropertyDescriptor PROP_CACHE_ENTRY_KEEP = new PropertyDescriptor.Builder()
            .name("The Cache Entry to keep")
            .description("If we remove a duplicate using a pattern then we have the option to keep "
            + "a specific key (actually, to add it back after pattern removal).")
            .required(false)
            .addValidator(StandardValidators.createAttributeExpressionLanguageValidator(AttributeExpression.ResultType.STRING, true))
            .defaultValue('${uniqueID}')
            .build()

    public static final Relationship REL_SUCCESS = new Relationship.Builder()
            .name("success")
            .description("If the cache was successfully communicated with it will be routed to this relationship")
            .build()
    public static final Relationship REL_NOT_FOUND = new Relationship.Builder()
            .name("not-found")
            .description("If a FlowFile's Cache Entry Identifier was not found in the cache, it will be routed to this relationship")
            .build()
    public static final Relationship REL_FAILURE = new Relationship.Builder()
            .name("failure")
            .description("If unable to communicate with the cache or if the cache entry is evaluated to be blank, the FlowFile will be penalized and routed to this relationship")
            .build()
    private final Set<Relationship> relationships

    private static class StringSerializer implements Serializer<String>{
    //private final Serializer<String> keySerializer = new Serializer<String>() {
        @Override
        void serialize(String value, OutputStream output) throws SerializationException, IOException {
            output.write(value.getBytes(StandardCharsets.UTF_8))
        }
    }


    // CacheValue is for storing some decription and time in the value
    private static class CacheValue{

        private final String description;
        private final long entryTimeMS;

        public CacheValue(String description, long entryTimeMS) {
            this.description = description;
            this.entryTimeMS = entryTimeMS;
        }

        public String getDescription() {
            return description;
        }

        public long getEntryTimeMS() {
            return entryTimeMS;
        }

    }

    // We need a CacheValueSerializer to serialize the values for the cache
    private static class CacheValueSerializer implements Serializer<CacheValue> {
        @Override
        void serialize(final CacheValue entry, final OutputStream out) throws SerializationException, IOException {
            long time = entry.getEntryTimeMS();
            byte[] writeBuffer = new byte[8];
            writeBuffer[0] = (byte) (time >>> 56);
            writeBuffer[1] = (byte) (time >>> 48);
            writeBuffer[2] = (byte) (time >>> 40);
            writeBuffer[3] = (byte) (time >>> 32);
            writeBuffer[4] = (byte) (time >>> 24);
            writeBuffer[5] = (byte) (time >>> 16);
            writeBuffer[6] = (byte) (time >>> 8);
            writeBuffer[7] = (byte) (time);
            out.write(writeBuffer, 0, 8);
            out.write(entry.getDescription().getBytes(StandardCharsets.UTF_8));
        }
    }


    RemoveCache() {
        final Set<Relationship> rels = new HashSet<>()
        rels.add(REL_SUCCESS)
        rels.add(REL_FAILURE)
        relationships = Collections.unmodifiableSet(rels)
    }

    List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        final List<PropertyDescriptor> descriptors = new ArrayList<>()
        descriptors.add(ScriptingComponentUtils.SCRIPT_FILE)
        descriptors.add(ScriptingComponentUtils.SCRIPT_BODY)
        descriptors.add(ScriptingComponentUtils.MODULES)
        descriptors.add(PROP_CACHE_ENTRY_IDENTIFIER)
        descriptors.add(PROP_DISTRIBUTED_CACHE_SERVICE)
        descriptors.add(PROP_CACHE_ENTRY_REMOVAL_TYPE)
        descriptors.add(PROP_CACHE_ENTRY_KEEP)
        return descriptors
    }

    Set<Relationship> getRelationships() {
        return relationships
    }

    void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get()
        if (flowFile == null) {
            return
        }

        final ComponentLog logger = getLogger()
        final String cacheKey = context.getProperty(PROP_CACHE_ENTRY_IDENTIFIER).evaluateAttributeExpressions(flowFile).getValue()

        final String removalType = context.getProperty(PROP_CACHE_ENTRY_REMOVAL_TYPE).evaluateAttributeExpressions(flowFile).getValue()
        final String idToKeep = context.getProperty(PROP_CACHE_ENTRY_KEEP).evaluateAttributeExpressions(flowFile).getValue()

        final Serializer<String> keySerializer = new StringSerializer()
        final Serializer<CacheValue> valueSerializer = new CacheValueSerializer()

        if (StringUtils.isBlank(cacheKey)) {
            logger.error("FlowFile {} has no attribute for given Cache Entry Identifier", [flowFile])
            flowFile = session.penalize(flowFile)
            session.transfer(flowFile, REL_FAILURE)
            return
        }

        final DistributedMapCacheClient cache = context.getProperty(PROP_DISTRIBUTED_CACHE_SERVICE).asControllerService(DistributedMapCacheClient.class)
        try {
            // This removes a key by the full cache
            if ( removalType == "Full" )
            {
              cache.remove(cacheKey, keySerializer)
            }
            else
            {
              logger.info("The id to keep is {}", idToKeep)

              // Was running checks for declaredMethods
              /*
              cache.class.declaredMethods
              .findAll { !it.synthetic }
              .each { logger.info( "$it.name $it.parameters.name") }
              */


              // for the cach value, we are just going to add current time and the filename
              // for the flowfile.
              final Long timeMS = System.currentTimeMillis()
              CacheValue valuetoKeep = new CacheValue(flowFile.getAttribute('filename'), timeMS)
              logger.info ("CacheValue = {}", valuetoKeep)

              long countRemoved = cache.removeByPattern( cacheKey )
              logger.info( "Removed {} records with pattern cachekey = {}", countRemoved, cacheKey)
              if ( idToKeep != "")
              {
                cache.putIfAbsent(idToKeep, valuetoKeep, keySerializer, valueSerializer)
              }

            }

            session.transfer(flowFile, REL_SUCCESS)
        } catch (final IOException e) {
            flowFile = session.penalize(flowFile)
            session.transfer(flowFile, REL_FAILURE)
            logger.error("Unable to communicate with cache when processing {} due to {}", [flowFile, e])
        }
    }



}

processor = new RemoveCache()
