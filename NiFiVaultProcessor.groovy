/**
The purpose of this script it to be used as a NiFi InvokeScriptedProcessor.

This particular processor will:

    - take a role id and fetch the secret id
    - use that secret to fetch a token, which  goes into the cluster state
    - make sure we grab the lease during for the token, and keep that in the cluster state
    - utilize our token to make requests to look up a secret, the path for the
      secret is a property that can be filled with a value or a flowfile attribute
    - return the secret in the success response or as attributes
    - make sure the token is still valid, if not make a new token request
    - pass any failures on to the failure path

*/

// get all of our NiFi imports
import org.apache.nifi.expression.AttributeExpression
import org.apache.nifi.flowfile.FlowFile
import org.apache.nifi.logging.ComponentLog
import org.apache.nifi.processor.Relationship
import org.apache.nifi.processor.exception.ProcessException
import org.apache.nifi.components.PropertyDescriptor
import org.apache.nifi.processor.util.StandardValidators
import org.apache.nifi.script.ScriptingComponentUtils
import java.nio.charset.StandardCharsets
import org.apache.nifi.ssl.SSLContextService

// We need this for storing state for the processor
import org.apache.nifi.components.state.Scope

// used for init (if we use it)
import org.apache.nifi.processor.ProcessorInitializationContext


// We are going to need some Vault related classes
/*
  It is faster to just get this driver and add it to the groovy classpath
  or utilize the modules property of the script.  Grape seems to add a fair
  bit of load time ... plus Grape may not work if we don't have Ivy installed.
  @Grab('com.bettercloud:vault-java-driver:3.0.0')

  You can also use the modules section of the processor to define where any
  libraries might be.
*/
import com.bettercloud.vault.VaultConfig
import com.bettercloud.vault.SslConfig
import com.bettercloud.vault.Vault
import com.bettercloud.vault.VaultException
import com.bettercloud.vault.response.LogicalResponse
import com.bettercloud.vault.api.Auth
import com.bettercloud.vault.response.AuthResponse

// We need these for adding the lease duration to the token issue time.
import groovy.time.TimeDuration
import groovy.time.TimeCategory

import org.apache.nifi.components.ValidationContext;
import org.apache.nifi.components.ValidationResult;

// The base class, extends AbstractProcessor
class NiFiVaultProcessor extends AbstractProcessor {

    // Add the properties
    public static final PropertyDescriptor PROP_ROLE_NAME = new PropertyDescriptor.Builder()
            .name("App Role Name")
            .description("The App Role Name the processor should use to access the secret store.")
            .required(true)
            .addValidator(StandardValidators.createAttributeExpressionLanguageValidator(AttributeExpression.ResultType.STRING, true))
            .defaultValue('nifi')
            .expressionLanguageSupported(true)
            .build()

    public static final PropertyDescriptor PROP_INITIAL_TOKEN = new PropertyDescriptor.Builder()
            .name("Initial Access Token")
            .description("The initial token required to retrieve the token for the AppRole.")
            .required(true)
            .addValidator(StandardValidators.createAttributeExpressionLanguageValidator(AttributeExpression.ResultType.STRING, true))
            .defaultValue('nifi')
            .expressionLanguageSupported(true)
            .build()


    public static final PropertyDescriptor PROP_VAULT_SERVER = new PropertyDescriptor.Builder()
            .name("The Vault server address and port")
            .description("The location and address of the server that should be used to look up secrets.")
            .required(true)
            .addValidator(StandardValidators.createAttributeExpressionLanguageValidator(AttributeExpression.ResultType.STRING, true))
            .defaultValue('https://localhost:8200')
            .build()

    public static final PropertyDescriptor PROP_SECRET_LOOKUP = new PropertyDescriptor.Builder()
            .name("The secret to request from Vault.")
            .description("The secret to request from Vault.")
            .required(true)
            .addValidator(StandardValidators.createAttributeExpressionLanguageValidator(AttributeExpression.ResultType.STRING, true))
            .defaultValue('${secret}')
            .expressionLanguageSupported(true)
            .build()

    // We need to support SSL/TLS
    public static final PropertyDescriptor PROP_SSL_CONTEXT_SERVICE = new PropertyDescriptor.Builder()
            .name("SSL Context Service")
            .description("The SSL Context Service used to provide client certificate information for TLS/SSL (https) connections.")
            .required(false)
            .identifiesControllerService(SSLContextService.class)
            .build();

    // Define the attributes that are appriate for destinations
    public static final String DESTINATION_ATTRIBUTES = "flowfile-attributes"
    public static final String DESTINATION_CONTENT = "flowfile-content"
    public static final PropertyDescriptor PROP_DESTINATION = new PropertyDescriptor.Builder()
            .name("Destination")
            .description("Indicates whether the results of the Vault request are written to the FlowFile content or FlowFile attributes. "
                    + "if using attributes, one attribute is created and written for each key/value pair found in the secret.")
            .required(true)
            .allowableValues(DESTINATION_CONTENT, DESTINATION_ATTRIBUTES)
            .defaultValue(DESTINATION_CONTENT)
            .build();

    // Add the relationships
    public static final Relationship REL_SUCCESS = new Relationship.Builder()
            .name("success")
            .description("If the secret was successfully obtained it will be routed to this relationship")
            .build()

    public static final Relationship REL_FAILURE = new Relationship.Builder()
            .name("failure")
            .description("If unable to communicate with the secret store, the FlowFile will be penalized and routed to this relationship")
            .build()

    private final Set<Relationship> relationships


    // The class instantiation
    NiFiVaultProcessor() {
        final Set<Relationship> rels = new HashSet<>()
        rels.add(REL_SUCCESS)
        rels.add(REL_FAILURE)
        relationships = Collections.unmodifiableSet(rels)
    }

    // Add the descriptors
    List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        final List<PropertyDescriptor> descriptors = new ArrayList<>()
        descriptors.add(ScriptingComponentUtils.SCRIPT_FILE)
        descriptors.add(ScriptingComponentUtils.SCRIPT_BODY)
        descriptors.add(ScriptingComponentUtils.MODULES)
        descriptors.add(PROP_ROLE_NAME)
        descriptors.add(PROP_INITIAL_TOKEN)
        descriptors.add(PROP_VAULT_SERVER)
        descriptors.add(PROP_SECRET_LOOKUP)
        descriptors.add(PROP_SSL_CONTEXT_SERVICE)
        descriptors.add(PROP_DESTINATION)
        return descriptors
    }

    // And the relationships
    Set<Relationship> getRelationships() {
        return relationships
    }

    @Override
    protected Collection<ValidationResult> customValidate(final ValidationContext context) {
        final Collection<ValidationResult> results = new ArrayList<>();

        if (context.getProperty(PROP_VAULT_SERVER).evaluateAttributeExpressions().getValue().startsWith("https") && context.getProperty(PROP_SSL_CONTEXT_SERVICE).getValue() == null) {
            results.add(new ValidationResult.Builder()
                    .explanation("URL is set to HTTPS protocol but no SSLContext has been specified")
                    .valid(false)
                    .subject("SSL Context")
                    .build());
        }


        return results;
    }

    /*
      An internal ClientToken object that holds the token and the duration
      of the lease, in seconds.
    */
    private static class ClientToken{

        private final String token;
        private final long leaseDuration;

        public ClientToken(String token, long leaseDuration) {
            this.token = token;
            this.leaseDuration = leaseDuration;
        }

        public String getToken() {
            return token;
        }

        public long getLeaseDuration() {
            return leaseDuration;
        }

    }

    /*
      The target format of dates we are using, utilized in multiple places,
      so it ends up at the class level.
    */
    //private final String targetFormat = "EEE d MMM HH:mm:ss 'UTC' yyyy"

    /**
     * Get a role id for the rolename that is passed.
     *
     * @param roleName  The name of the role
     * @param vault     The vault instance from which we should make the request
     */
    private final String getRoleID( String roleName, Vault vault ){
      return vault.logical().read("auth/approle/role/$roleName/role-id").getData().get('role_id')

    }


    /**
     * Get the secret for a particalar role
     *
     * @param roleName  the name of the role
     * @param vault     a vault instance
     */
    private final String getRoleSecret( String roleName, Vault vault ){
      return vault.logical().write("auth/approle/role/$roleName/secret-id", null)
                  .getData().get('secret_id')
    }


    /**
     * Get the client token to use for working with secrets
     *
     * @param context  the ProcessContext of the processor
     */
    private final String getClientToken(final ProcessContext context, final FlowFile flowFile)
    {
      // We go ahead and grab the rolename, as we need it for getting a clientToken.
      final String roleName = context.getProperty(PROP_ROLE_NAME).evaluateAttributeExpressions(flowFile).getValue()

      // Set up the state manager so we can read the state and see what we need.
      // and use that to look up the current state
      def stateManager = context.stateManager
      def currentState = stateManager.getState(Scope.CLUSTER)

      // set up our vault object using the initial token, which is why we pass nuyll to the config
      Vault vaultAuth = new Vault( getNewVaultConfig(context, flowFile, null) )

      // get our roleID, secretID
      def roleID = getRoleID(roleName, vaultAuth)
      def secretID = getRoleSecret(roleName, vaultAuth)

      // Get our clientToken, which we will add to the statemap and also return
      final AuthResponse authResponse = vaultAuth.auth().loginByAppRole('approle', roleID, secretID)
      ClientToken token = new ClientToken( authResponse.getAuthClientToken(), authResponse.getAuthLeaseDuration())


      // We also get the leaseDuration so we can add it all to the logging if needed.
      def clientToken = token.getToken()

      // This gives us lease duration, but token uses is important too
      int leaseDuration = token.getLeaseDuration()


      /*
        We want the issue time for the token as well.

        The Number of milliseconds return for the vault issue time can be
        different, so we trim them, otherwise it screws up the date format
      */
      def tokenIssueTime =  vaultAuth.logical().read("auth/token/lookup/$clientToken").getData().get('issue_time')
                            .replaceFirst(~/[\.][0-9]+/, "")

      def tokenUses = vaultAuth.logical().read("auth/token/lookup/$clientToken").getData().get('num_uses')


      // without the ms, this is the date formate we get
      def sourceFormat = "yyyy-MM-dd'T'HH:mm:ssXXX"

      // and this is format one we want
      def targetFormat = "EEE d MMM HH:mm:ss 'UTC' yyyy"
      def theDate = new Date().parse(sourceFormat, tokenIssueTime)

      /*
        If we can get the real expiration time from the logical request we
        should use that instead of the above
      */
      def expirationTime =  new Date().parse(sourceFormat,vaultAuth.logical().read("auth/token/lookup/$clientToken").getData().get('expire_time')
                            .replaceFirst(~/[\.][0-9]+/, ""))


      // Log it out in info, for troublshooting if needed.
      logger.info("\n"
                 +"Updated state with the token \n"
                 +"Token Issue Time         : {} \n"
                 +"Token Lease (s)          : $leaseDuration \n"
                 +"Token Expiration Time    : $expirationTime \n"
                 +"Total Token Uses Allowed : $tokenUses", theDate.format(targetFormat))

      // Create a new map, and put the clientToken and expiration in it, then update the Cluster State
      def newMap = ['clientToken': clientToken,
                    'tokenExpiration':expirationTime.format(targetFormat),
                    'tokenUsesAllowed':tokenUses,
                    'tokenUsesRemaining':tokenUses]

      if (currentState.version == -1)
      {
        stateManager.setState(newMap, Scope.CLUSTER);
      } else {
        stateManager.replace(currentState, newMap, Scope.CLUSTER);
      }

      currentState = stateManager.getState(Scope.CLUSTER)
      logger.info("\ncurrentstate after change:\n{}", currentState)

      // return the client token
      return clientToken

    }


    /**
     * Get a configuration object for Vault
     *
     * @param context   the context of the processor, allows us to get properties
     * @param token     the token we should use to make the request.  It can be null,
     *                  in which case we will use the supplied inital token (usually for auth)
     */
    private VaultConfig getNewVaultConfig( final ProcessContext context, final FlowFile flowFile, final String token )
    {
      try
      {
        def openTimeout = 5
        def readTimeout = 30

        /*
          If we are passed a token for lookup, then use it, otherwise use
          the initial token.  Reason being is that the token configured in the
          processor is used for the auth lookup.  All other tokens are client
          tokens used for secret lookup.

          Ternary - if token is not null, the tokenToUse is token, otherwise look it up
        */
        def tokenToUse = (token != null) ? token : context.getProperty(PROP_INITIAL_TOKEN).evaluateAttributeExpressions(flowFile).getValue()
        // Set up the SSL context (if there is one, otherwise null)
        final SSLContextService sslContextService = context.getProperty(PROP_SSL_CONTEXT_SERVICE).asControllerService(SSLContextService.class);
         // set up the config, using SSL if needed.

         if (sslContextService == null) {
             VaultConfig config = new VaultConfig()
                     .address(context.getProperty(PROP_VAULT_SERVER).getValue())
                     .token(tokenToUse)
                     .openTimeout(openTimeout)
                     .readTimeout(readTimeout)
                     .build();
         }
         else {
             VaultConfig config= new VaultConfig()
                     .address(context.getProperty(PROP_VAULT_SERVER).getValue())
                     .token(tokenToUse)
                     .openTimeout(openTimeout)
                     .readTimeout(readTimeout)
                     .sslConfig(new SslConfig().trustStoreResource(sslContextService.getTrustStoreFile()).keyStoreResource(sslContextService.getKeyStoreFile(), sslContextService.getKeyStorePassword()))
                     .build();
         }
      } catch( Exception e )
      {
        logger.error("We were unable to create the Config Object for Vault: $e")
      }
    }

    // onTrigger ... where the flowfile action happens
    void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {

        // Get our flowfile
        FlowFile flowFile = session.get()

        // Need to think about this .. can this be at the front of a flow?
        // If there is no flowFile, we need to create one
        if (flowFile == null) {
            flowFile = session.create()
        }

        // Set up our Logger
        final ComponentLog logger = getLogger()

        try
        {

          // Set up the state manager so we can read the state and see what we need.
          def stateManager = context.stateManager

          // and use that to look up the current state .. and get the token expiration
          def currentState = stateManager.getState(Scope.CLUSTER)

          // If we don't have a token, then we need to get one
          if (currentState == -1 || !currentState.toMap().containsKey('clientToken'))
          {
            // Get client token does return the token if we want it, but it also
            // adds the token to our state, so that we can use it later
            logger.info("need a token for the first time")
            getClientToken( context, flowFile )

            // refresh the currentstate now that we've updated the token
            currentState = stateManager.getState(Scope.CLUSTER)

          }

          // So at this point, we should have a currentState object if we didn't
          // before, so now we can check for expired or maxused token
          def tokenExpiration = currentState.toMap().get('tokenExpiration')

          // We have to set the timezone to get our current data and do the comparison
          TimeZone.setDefault(TimeZone.getTimeZone('UTC'))
          def targetFormat = "EEE d MMM HH:mm:ss 'UTC' yyyy"
          Date theExpiration = new Date().parse(targetFormat, tokenExpiration)
          Date nowDate = new Date()

          // Determine if our token is expired or not
          boolean expired = true
          if (nowDate.after(theExpiration))
          {
            expired = true
            logger.info("\nEvaluated the expiration ($theExpiration) vs now ($nowDate) and "
                       +"the token is expired.\n"
                       +"expired = $expired")

          } else
          {
            expired = false
            logger.info("\nEvaluated the expiration ($theExpiration) vs now ($nowDate) and "
                       +"the token is not expired.\n"
                       +"expired = $expired")
          }

          // Find out current number of uses remaining
          int tokenUsesRemaining = Integer.parseInt(currentState.toMap().get('tokenUsesRemaining'))

          if (expired || tokenUsesRemaining <= 0)
          {
            logger.info("need a new token")
            getClientToken( context, flowFile )

            // refresh the currentstate now that we've updated the token
            currentState = stateManager.getState(Scope.CLUSTER)

            //tokenUses remaining gets reset
            tokenUsesRemaining = Integer.parseInt(currentState.toMap().get('tokenUsesRemaining'))

          }


          // We have a token for sure now - use that token to make our secret request
          //currentState = stateManager.getState(Scope.CLUSTER)
          def tokenToUse = currentState.toMap().get('clientToken')
          logger.info ("token to use : $tokenToUse")

          // Set up our Vault object for making requests.
          Vault vaultSecrets = new Vault( getNewVaultConfig(context, flowFile, tokenToUse) )

          // Get the secret we are supposed to look up
          final String secret = context.getProperty(PROP_SECRET_LOOKUP).evaluateAttributeExpressions(flowFile).getValue()

          // Look to se if we want the secrets as attributes or content, and we can log it
          String destination = context.getProperty(PROP_DESTINATION).getValue()
          logger.info("destination: $destination")

          /*
             At this point, regardless of destination, we should check for errors.
             If we get an error, we should route to failure.

             There is no concept of "empty", you can't create a secret without
             any key/value pairs and if the secret doesn't exist you are going
             to get a 403 anyway.
          */
          if (DESTINATION_ATTRIBUTES.equals(destination))
          {
            def text = vaultSecrets.logical().read(secret).getData()

            text.each{ key, value -> flowFile = session.putAttribute(flowFile, key, value)}
            logger.info("Added secrets to attributes.")

          }
          else if (DESTINATION_CONTENT.equals(destination))
          {
            String text = vaultSecrets.logical().read(secret).getData()

            flowFile = session.write(flowFile, {outputStream ->
                outputStream.write(text.getBytes(StandardCharsets.UTF_8))
            } as OutputStreamCallback)
            logger.info("Added secrets to content.")

          }

          // subtract the token we just used
          // We need to decrease our token uses remaining and add it back to the Map
          Map<String,String> newMap = new HashMap<String, String>()
          currentState.toMap().each{key, value -> newMap.put(key, value)}
          tokenUsesRemaining = tokenUsesRemaining - 1
          logger.info("Token Uses Remaining : $tokenUsesRemaining")
          newMap.put('tokenUsesRemaining', tokenUsesRemaining.toString())
          stateManager.replace(currentState, newMap, Scope.CLUSTER);

          // Once we have what we need, make sure to pass it on to the success relationship
          session.transfer(flowFile, REL_SUCCESS)
          session.commit()


        // If we can't connection to Vault, make sure we penalize and follow the failure relationship.
        } catch (final Exception ex) {
            flowFile = session.penalize(flowFile)
            if (ex.message)
            {
              flowFile = session.write(flowFile, {outputStream ->
                  outputStream.write(ex.toString().getBytes(StandardCharsets.UTF_8))
              } as OutputStreamCallback)
              logger.error("Unable to communicate with Vault when processing {} due to {}", flowFile, ex.message)
            }
            else
            {
              String message = "Unable to communicate with Vault when processing $flowFile\n Error: $ex"
              flowFile = session.write(flowFile, {outputStream ->
                  outputStream.write(message.getBytes(StandardCharsets.UTF_8))
              } as OutputStreamCallback)
              logger.error(message)
            }
            session.transfer(flowFile, REL_FAILURE)
            session.commit()



        }


    }
  }
  // the instance
  processor = new NiFiVaultProcessor()
