/**
This script will connect to the theInputStreamtributedCache Service indicated
on the socket below and check for the key or keys that are listed
in "keys".

If the value returns empty, then the key is not present.
Otherwise we get the value (which is less interesting, than simply knowing
there is a value!)
*/

/*
Set up the protocol, the keys we are looking for, and the server.
There is only 1 protocol version, 1
*/
def protocolVersion = 1
def keys = ['227-WARNING', '221-OK', '452-UNKNOWN']
def server = 'master08.aws.staq-infra.com'
def port = 4557

// set up the serverSocker
serverSocket = new Socket( server, port )

// get our input and output streams
serverSocket.withStreams { input, output ->
  def theOutputStream = new DataOutputStream(output)
  def theInputStream = new DataInputStream(input)

  // Negotiate handshake/version
  theOutputStream.write('NiFi'.bytes)
  theOutputStream.writeInt(protocolVersion)
  theOutputStream.flush()

  /*
    Get the status of the stream, here are the available returns:

      - RESOURCE_OK (20): The server accepted the client's proposal of protocol name/version
      - DIFFERENT_RESOURCE_VERSION (21): The server accepted the client's proposal of protocol name but not the version
      - ABORT (255): The server aborted the connection

    We send our handshake
  */
  status = theInputStream.read()
  while(status == 21) {
     protocolVersion = theInputStream.readInt()
     theOutputStream.writeInt(protocolVersion)
     theOutputStream.flush()
     status = theInputStream.read()
  }

  // Go ahead and show the keys we want to find
  print "keys: " + keys +"\n"

  // and then get the values, if any
  keys.each {
      key = it.getBytes('UTF-8')
      theOutputStream.writeUTF('get')

      // create a byte array output stream and wite it to the outputstream
      def baos = new ByteArrayOutputStream()
      baos.write(key)
      theOutputStream.writeInt(baos.size())
      baos.writeTo(theOutputStream)
      theOutputStream.flush()

      // Read what we get back and write the results
      def length = theInputStream.readInt()
      def bytes = new byte[length]
      theInputStream.readFully(bytes)
      value = new String(bytes)
      if (value == "")
        println "'$it' is not present in Distributed cache"
      else
        println "$it = $value"
      //println "$it = ${new String(bytes)}"

  }

  // Make sure to close the stream
  theOutputStream.writeUTF("close");
  theOutputStream.flush();
}
