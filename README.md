NiFi Scripts
==========================

Background
----------

This repository holds a number of scripts that can be utilized within NiFi or as a way to test NiFi functionality.  For instance, scripts can be utilized within ExecuteScript or InvokeScriptedProcessor components.


Running The Scripts
----------------

How a script can be run depends upon the type of script and the intended run location. Scripts that are intended to run within a InvokeScriptedProcessor will only be able to run within a NiFi processor, as only the NiFi environment will provide the needed context.

Other scripts may be able to run within another run time environment.

Each script included in this repository are noted below and include a description of what they do and where they can run.

| Script| Language| Notes|
| ---|:---:|:---|
|CacheChecker |Groovy |Can run in any Groovy environment.</p> Used to check the NiFi DistributedMapCache|
|CacheRemover|Groovy|Runs in a NiFi InvokeScriptedProcessor. </p> Used to remove key from the NiFi DistributedMapCache either by removing an exact key or removing a collection of keys that match a regex.  If multiple keys are removed, you have the option of adding a single key back in place of the keys that were removed.|
|VaultTester|Groovy|Not a NiFi script but a sample for how to work with a local vault instance that is leveraged later in the NiFiVaultProcessor|
|VaultAppRoleTester|Groovy|Builds upon the VaultTester, but utilizes an AppRole in a flow that can serve as an even better example for the NiFiVaultProcessor (another building block)|
|NiFiVaultProcessor|Groovy|Runs in a NiFi InvokeScriptedProcessor. </p> Used to remove key from the NiFi DistributedMapCache either by removing an exact key or removing a collection of keys that match a regex.  If multiple keys are removed, you have the option of adding a single key back in place of the keys that were removed.|
