/**
This script is a simple test of a Vault server to ensure that
you can connect and get the contents of a secret.

As set up below, it assumes a local vault instance that has already
been populated with a secret as described on the vault site:
https://www.vaultproject.io/intro/getting-started/first-secret.html

That secret contains data for the keys 'value' and 'excited', but not for 'test'.

Your localhost token will surely be different than the example below.
*/

/*
The script depends on the driver below.  Any system with Grapes and Ivy intalled
will work with the @Grab statement, otherwise you'll need to get the library
and place it on the classpath.  You can see some instructions here:
https://stackoverflow.com/questions/32180388/import-java-library-groovy

The @Grab statement utilizes Grape:
http://docs.groovy-lang.org/latest/html/documentation/grape.html

* Note:  Locally, I've noticed that using Grape seems kind of slow.  I improved
         the speed by copying the vault driver folder from the ~/.groovy/grapes directory
         to the ~/.groovy/lib directory (then you can comment out the @Grab)
*/
//@Grab('com.bettercloud:vault-java-driver:3.0.0')
import com.bettercloud.vault.VaultConfig
import com.bettercloud.vault.Vault
import com.bettercloud.vault.VaultException
import com.bettercloud.vault.response.LogicalResponse
import com.bettercloud.vault.api.Auth
import com.bettercloud.vault.response.AuthResponse

import groovy.time.TimeDuration
import groovy.time.TimeCategory

// Set up our config information
def server = 'http://localhost'
def port = 8200
def openTimeout = 5
def readTimeout = 30
def token = 'bd19391c-68cc-7060-fda2-d2e59d11c28f'
def secret = 'secret/hello'
def keys = ['value', 'excited', 'test']

// set up the VaultConfig
final VaultConfig config = new VaultConfig()
      .address( "$server:$port" )
      .token( token )
      .openTimeout( openTimeout )
      .readTimeout( readTimeout )
      .build()

//create our vault instance
Date startTime = new Date()
final Vault vault = new Vault(config)
Date endTime = new Date()

println TimeCategory.minus(endTime, startTime)

// Get the info
keys.each {
    // 'it' is a fun keyword for Groovy
    key = it
    value = vault.logical().read(secret).getData().get(key)
    if (!value)
      println "'$it' is not present in the secret store"
    else
      println "$it = $value"
}

// Testing getting a role id for an approle set up as described here:
// https://www.vaultproject.io/docs/auth/approle.html
roleID = vault.logical().read('auth/approle/role/testrole/role-id').getData().get('role_id')
// you could also get the whole thing with just .getData()
println "RoleID = $roleID"

//Get the secretID and accessor? vault write -f auth/approle/role/testrole/secret-id
final LogicalResponse response = vault.logical().write('auth/approle/role/testrole/secret-id', null)
String secretID = response.getData().get('secret_id')
String secretIDAccessor = response.getData().get('secret_id_accessor')
println "Secret ID: $secretID, SecretID Accessor: $secretIDAccessor"


// Get the token dependent upon the approle role id and secret id
final AuthResponse authResponse = vault.auth().loginByAppRole('approle', roleID, secretID)
String authToken = authResponse.getAuthClientToken()
boolean isRenewable = authResponse.isAuthRenewable()
long leaseDuration = authResponse.getAuthLeaseDuration()
String userName = authResponse.getUsername()
boolean renewable = authResponse.getRenewable()

println "Auth Token: $authToken, renewable?: $isRenewable, Lease Duration: $leaseDuration, getRenewable$renewable, User Name: $userName"

// new Logical
